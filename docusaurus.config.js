module.exports = {
  title: 'Resquare UI',
  tagline: 'Declarative Spigot Plugin UI Library',
  url: 'https://resquare.reactant.dev',
  baseUrl: '/',
  favicon: 'img/favicon.ico',
  organizationName: 'Reactant Dev', // Usually your GitHub org/user name.
  projectName: 'Resquare', // Usually your repo name.
  onBrokenLinks: 'warn',
  themeConfig: {
    colorMode: {
      defaultMode: 'light',
      disableSwitch: false,
      respectPrefersColorScheme: true,
    },
    navbar: require('./navbar.js'),
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Discord',
              href: 'https://discord.gg/9NufxVr',
            },
          ],
        },
        {
          title: 'Social',
          items: [
            {
              label: 'GitLab',
              href: 'https://gitlab.com/reactant/resquare',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Reactant Dev`,
    },
    prism: {
      theme: require('prism-react-renderer/themes/nightOwlLight'),
      darkTheme: require('prism-react-renderer/themes/nightOwl'),
      additionalLanguages: ['kotlin'],
    },
    gtag: {
      trackingID: 'G-77KWJBD3PT',
      anonymizeIP: true,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://gitlab.com/reactant/resquare.reactant.dev/-/edit/master/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.scss'),
        },
      },
    ],
  ],
  plugins: [
    'docusaurus-plugin-sass',
  ],
};
