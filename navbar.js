/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

module.exports = {
  title: 'Resquare UI',
  logo: {
    alt: 'Resquare Logo',
    src: 'img/logo.png',
  },
  items: [
    {
      to: 'docs/introduction',
      activeBasePath: 'docs',
      label: 'Docs', position: 'left',
    },
    {href: 'https://reactant.dev', label: 'Reactant', position: 'right'},
    {href: 'https://gitlab.com/reactant/resquare', label: 'GitLab', position: 'right'},
  ],
  hideOnScroll: true,
};
