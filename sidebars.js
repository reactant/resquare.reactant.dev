/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

module.exports = {
  resquareSidebar: [
    'introduction',
    'getting-started',
    {
      type: 'category',
      label: 'Guide',
      collapsed: false,
      items: [
        'guide/component',
        'guide/render-optimization',
        'guide/hooks',
        'guide/styles',
        'guide/multithreaded-rendering'
      ],
    },
    'best-practice',
  ],
};
