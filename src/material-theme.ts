import { createMuiTheme } from "@material-ui/core";

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#2a8f7c",
    },
    secondary: {
      main: "#a170a5",
    },
  },
});
