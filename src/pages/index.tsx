import React, { FC } from 'react';
import Layout from '@theme/Layout';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';

const Home: FC = () => {
  const context = useDocusaurusContext();

  return (
    <Layout description="Reactant documentation">
      WIP
    </Layout>
  );
};

export default Home;
